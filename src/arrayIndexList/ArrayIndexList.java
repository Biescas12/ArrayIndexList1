package arrayIndexList;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import indexList.IndexList;

public class ArrayIndexList<E> implements IndexList<E> {
	private static final int INITCAP = 1; 
	private static final int CAPTOAR = 1; 
	private static final int MAXEMPTYPOS = 2; 
	private E[] element; 
	private int size; 

	public ArrayIndexList() { 
		element = (E[]) new Object[INITCAP]; 
		size = 0; 
	} 
	
	public int capacity() { 
		 return element.length; 
	}

	public void add(int index, E e) throws IndexOutOfBoundsException {
		if(index < 0 || index > this.size()) {
			throw new IndexOutOfBoundsException("add: Invalid index = " + index);
		}
		
		if (this.size() == this.element.length) {
			this.changeCapacity(CAPTOAR);
		}
		
		moveDataOnePositionTR(index, this.size()-1);
		
		this.element[index] = e;
		this.size++;
	}


	public void add(E e) {
		if (this.size() == this.element.length) {
			this.changeCapacity(CAPTOAR);
		}
		this.element[this.size()] = e;
		this.size++;
	}


	public E get(int index) throws IndexOutOfBoundsException {
		if(index < 0 || index > this.size()) {
			throw new IndexOutOfBoundsException("get: Invalid index = " + index);
		}
		return this.element[index]; 
	}


	public boolean isEmpty() {
		return size == 0;
	}


	public E remove(int index) throws IndexOutOfBoundsException {
		if(index < 0 || index >= this.size()) {
			throw new IndexOutOfBoundsException("remove: Invalid index = " + index);
		}
		E temp = this.element[index];
		
		moveDataOnePositionTL(index+1, this.size-1);
		
		this.element[this.size()-1] = null;
		
		this.size--;
		
		if(this.element.length - this.size() > MAXEMPTYPOS) {
			this.changeCapacity(-CAPTOAR);
		}
		
		return temp;
	}


	public E set(int index, E e) throws IndexOutOfBoundsException {
		if(index < 0 || index >= this.size()) {
			throw new IndexOutOfBoundsException("set: Invalid index = " + index);
		}		
		E temp = this.element[index];
		this.element[index] = e;
		return temp;
	}


	public int size() {
		return size;
	}	
	
	
	
	// private methods  -- YOU CAN NOT MODIFY ANY OF THE FOLLOWING
	// ... ANALYZE AND USE WHEN NEEDED
	
	// you should be able to decide when and how to use
	// following method.... BUT NEED TO USE THEM WHENEVER
	// NEEDED ---- THIS WILL BE TAKEN INTO CONSIDERATION WHEN GRADING
	
	private void changeCapacity(int change) { 
		int newCapacity = element.length + change; 
		E[] newElement = (E[]) new Object[newCapacity]; 
		for (int i=0; i<size; i++) { 
			newElement[i] = element[i]; 
			element[i] = null; 
		} 
		element = newElement; 
	}
	
	// useful when adding a new element with the add
	// with two parameters....
	private void moveDataOnePositionTR(int low, int sup) { 
		// pre: 0 <= low <= sup < (element.length - 1)
		for (int pos = sup; pos >= low; pos--)
			element[pos+1] = element[pos]; 
	}

	// useful when removing an element from the list...
	private void moveDataOnePositionTL(int low, int sup) { 
		// pre: 0 < low <= sup <= (element.length - 1)
		for (int pos = low; pos <= sup; pos++)
			element[pos-1] = element[pos]; 
	}


	// The following two methods are to be implemented as part of an exercise
	public Object[] toArray() {
		Object [] array = new Object[this.size];
		
		for(int i = 0; i < this.size; i++) {
			array[i] = this.element[i];
		}
		return array;
	}


	@SuppressWarnings("unchecked")
	public <T1> T1[] toArray(T1[] array) {
		if(this.size > array.length) {
			array = (T1[]) Array.newInstance(array.getClass().getComponentType(), this.size);
		}
		else {
			for (int i = this.size; i <array.length; i++) {
				array[i] = null;
			}
		}
		
		for (int i =0; i<this.size; i++) {
			array[i] = (T1) this.element[i];
		}
		
		return array;
	}
	
	public void clear() {
		for(int i =0; i<= this.size; i++) {
			this.element[i] = null;
		}
	}
	
	public Object[] clone(Object[] list) {
		Object[] copy = list.clone();
		return copy;
	}
	
	@SuppressWarnings("null")
	public List<E> subList(int fromIndex, int toIndex) {
		
		List<E> newList = null;
		if (fromIndex < 0 || toIndex > size || fromIndex > toIndex) {
			throw new IndexOutOfBoundsException();
		}
		
		for(int i = fromIndex; i < toIndex; i++) {
			newList.add(this.element[i]);
		}
		return newList;
		
	}

}
